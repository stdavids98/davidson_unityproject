﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
	public float xMin, xMax, zMin, zMax;
}

[System.Serializable]
public class Ammo
{
	public int ammoCount = 20;
}

public class PlayerController : MonoBehaviour 
{
	private Rigidbody rb;
	private AudioSource audioSource;
	private float nextFire;

	public float speed; 
	public float tilt;
	public Boundary boundary;
	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;
	public Ammo ammo;

	public GUIText ammoText;

	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		audioSource = GetComponent<AudioSource> ();
		print (ammo.ammoCount);
	}

	void Update ()
	{

		if (Input.GetButton("Fire1") && Time.time > nextFire)
		{
			if (ammo.ammoCount == 0)
			{
				return;
			}
			nextFire = Time.time + fireRate;
			Instantiate (shot, shotSpawn.position, shotSpawn.rotation);
			audioSource.Play ();
			ammo.ammoCount -= 1;

		}

		ammoText.text = "Ammo: " + ammo.ammoCount;
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.velocity = movement*speed;

		rb.position = new Vector3 
			(
				Mathf.Clamp (rb.position.x, boundary.xMin, boundary.xMax),
				0.0f,
				Mathf.Clamp (rb.position.z, boundary.zMin, boundary.zMax)
			);
		rb.rotation = Quaternion.Euler (0.0f, 0.0f, rb.velocity.x * -tilt);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Pickup")) 
		{
			ammo.ammoCount += 20;
			print (ammo.ammoCount);
			GameObject.Destroy(other.gameObject);
		}
	}

}

