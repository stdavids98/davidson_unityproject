﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour 
{
	private Rigidbody rb;

	public GameObject explosion;
	public GameObject playerExplosion;
	public int scoreValue;
	private GameController gameController;

	public GameObject pickupPrefab;

	void Start () 
	{
		rb = GetComponent<Rigidbody>();

		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		if (gameController == null) 
		{
			Debug.Log ("Cannot find 'GameController' script");
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Boundary") || other.CompareTag  ("Enemy") || other.CompareTag  ("Pickup") || other.CompareTag  ("Asteroid")) 
		{
			return;
		}

		if (explosion != null) 
		{
			Instantiate (explosion, transform.position, transform.rotation);

		}

		if (other.CompareTag ("Player")) 
		{
			Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
			gameController.GameOver ();
		}


		gameController.AddScore (scoreValue);
		Destroy (other.gameObject);
		Destroy (gameObject);
		if (CompareTag  ("Enemy"))
		{
		if (Random.value < .5f) 
		{
			Instantiate (pickupPrefab, transform.position, pickupPrefab.transform.rotation);
		}
		}

		

	}
}